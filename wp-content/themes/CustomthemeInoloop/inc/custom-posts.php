<?php

function menu_noticias_eventos() {
    $args = array(
         'public' => true,
        'menu_icon' => 'dashicons-media-document',
         'label'  => 'Noticias y eventos',
        'labels'      => array(
        'singular_name'     => _('Noticias y eventos'),
        'name_admin_bar'    => _('Nueva'),
        'add_new'           => __('Agregar nuevo'),
        'new_item'          => __('Nueva noticia y/o evento'),
        'add_new_item'      => __('Agregar noticia y/o evento'),
        'edit'              => __('Editar'),
        'edit_item'         => __('Editar noticia y/o evento'),
        ),
        'supports'=>array('title', 'thumbnail', 'editor'),
     );
     register_post_type( 'noticias-post', $args );
    $args = array(
         'hierarchical'      => true,
         'show_ui'           => true,
        'show_admin_column' => true,
        'label'             => 'Tipo(s) de post',
        'query_var'         => true,
        'labels'      => array(
        'all_items'         => __('Tipos de post'),
        )
     );
   
    register_taxonomy('noticia', array('noticias-post'), $args );
 }
 add_action( 'init', 'menu_noticias_eventos' );
 

 function menu_video() {
   $args = array(
        'public' => true,
       'menu_icon' => 'dashicons-format-video',
        'label'  => 'Videos',
       'labels'      => array(
       'singular_name'     => _('Videos'),
       'name_admin_bar'    => _('Nueva'),
       'add_new'           => __('Agregar nuevo'),
       'new_item'          => __('Nuevo video'),
       'add_new_item'      => __('Agregar video'),
       'edit'              => __('Editar'),
       'edit_item'         => __('Editar video'),
       ),
       'supports'=>array('title', 'editor'),
    );
    register_post_type( 'video-post', $args );
   $args = array(
        'hierarchical'      => true,
        'show_ui'           => true,
       'show_admin_column' => true,
       'label'             => 'Tipo de video',
       'query_var'         => true,
       'labels'      => array(
       'all_items'         => __('Tipo de video'),
       )
    );
  
   register_taxonomy('video', array('videos-post'), $args );
}
add_action( 'init', 'menu_video' );

?>