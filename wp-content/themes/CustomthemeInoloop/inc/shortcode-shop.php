<?php
function productos_cat($atts){
    $param = shortcode_atts( array (
        'term' => 'term',
        'posts_number' => '-1'
        ), $atts );

    global $post;
    $args = array(
        'post_type'             => 'product',
        'post_status'           => 'publish',
        'ignore_sticky_posts'   => 1,
        'posts_per_page'        => $param['posts_number'],
        'tax_query'             => array(
            array(
                'taxonomy'      => 'product_cat',
                'field' => 'term_id', //This is optional, as it defaults to 'term_id'
                'terms'         => $param['term'],
                'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
            ),
            array(
                'taxonomy'      => 'product_visibility',
                'field'         => 'slug',
                'terms'         => 'exclude-from-catalog', // Possibly 'exclude-from-search' too
                'operator'      => 'NOT IN'
            )
        )
    );

$query = new WP_Query( $args );

// The Loop
while ( $query->have_posts() ) {
    $query->the_post();?>
<li class="product ">
    <a href="<?php echo get_permalink(); ?>">
        
        <?php
        if(has_post_thumbnail()){
            echo the_post_thumbnail('woocommerce_thumbnail');
        }
        else{
            ?>
            <img style="max-width:300px" src="../wp-content/themes/CustomthemeInoloop/images/woocommerce-placeholder-300x300.png" alt="Imagen producto">
            <?php
        }
        ?>
        <h2>
            <?php echo get_the_title(); ?>
        </h2>
        <div class="price">
            <?php
                if(get_post_meta( get_the_ID(), '_price', true )!=null){

                echo "S/".number_format(get_post_meta( get_the_ID(), '_price', true ), 2);

            }
                ?>
        </div>
        <a href="" class="button">
            Añadir al carrito
        </a>
    </a>
</li>

<?php } 


}

add_shortcode('productos_cat', 'productos_cat');


function productos_destacados(){
    global $post;
    $args = array(
        'post_type'             => 'product',
        'post_status'           => 'publish',
        'ignore_sticky_posts'   => 1,
        'posts_per_page'        => -1,
        'orderby'     => 'date',
        'order'       => 'DESC' ,
        'tax_query'  => array(
            array(
                'taxonomy' => 'product_visibility',
                'field'    => 'name',
                'terms'     => 'featured',
            )
        )
     );

$query = new WP_Query( $args );
// The Loop
while ( $query->have_posts() ) {
    $query->the_post();?>
<li class="product ">
    <a href="<?php echo get_permalink(); ?>">
        <?php
                echo the_post_thumbnail('woocommerce_thumbnail');
            ?>
        <h2>
            <?php echo get_the_title(); ?>
        </h2>
        <div class="price">
            <?php
                if(get_post_meta( get_the_ID(), '_price', true )!=null){

                echo "S/".number_format(get_post_meta( get_the_ID(), '_price', true ), 2);

            }
                ?>
        </div>
        <a href="" class="button">
            Añadir al carrito
        </a>
    </a>
</li>

<?php } 
}
add_shortcode('productos_destacados', 'productos_destacados');





function categories_shop(){
    global $post;
    $args = array(
        'taxonomy'   => "product_cat",
        'exclude' => "uncategorized",
        'parent'=>0

    );
    $product_categories = get_terms($args);

    foreach( $product_categories as $cat ){
        ?>
<a class="category grid cx" href="<?php echo get_term_link( $cat ); ?>">
    <img src=" <?php echo wp_get_attachment_url(get_term_meta( $cat->term_id, 'thumbnail_id', true )); ?>"
        alt="categories">
    <div class="grid cx cy tac">
        <?php
        echo $cat->name;
    ?>
    </div>
</a>
<?php
        } 
    }

add_shortcode('categories_shop', 'categories_shop');


?>