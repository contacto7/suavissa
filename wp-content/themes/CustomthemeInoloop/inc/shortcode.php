<?php

function noticias_eventos(){
    global $post;
    $args = array(
        'post_type'             => 'noticias-post',
        'post_status'           => 'publish',
        'ignore_sticky_posts'   => 1,
        'posts_per_page'        => -1,
        'orderby'     => 'date',
        'order'       => 'DESC' ,
 
     );

$query = new WP_Query( $args );
// The Loop
if ( $query->have_posts() ) {?>
    <div class="titulo">
    <h2>NOTICIAS Y DESTACADOS</h2>
    <h3>Lo mejor para ti</h3>
</div>

<ul class="grid cx ">
<?php
while ( $query->have_posts() ) {
    $query->the_post();?>
<li class="new grid">
    <a href="<?php echo get_permalink(); ?>">
        <?php
                echo the_post_thumbnail();
            ?>
    </a>
    <div>
        <a href="<?php echo get_permalink(); ?>">
            <h3>
                <?php echo get_the_title(); ?>
            </h3>
        </a>
        <p>
            <?php
                echo get_the_excerpt(); 
                
                ?>

        </p>
        <a href="<?php echo get_permalink(); ?>" class="button">
            Ver más
        </a>
    </div>

</li>

<?php }?>
</ul>
<?php
}
}
add_shortcode('noticias_eventos', 'noticias_eventos');


function capacitaciones(){
    global $post;
    $args = array(
        'post_type'             => 'video-post',
        'post_status'           => 'publish',
        'ignore_sticky_posts'   => 1,
        'posts_per_page'        => -1,
        'orderby'     => 'date',
        'order'       => 'DESC' ,
 
     );

$query = new WP_Query( $args );
// The Loop
while ( $query->have_posts() ) {
    $query->the_post();?>
<li class="video grid ">

    <?php             
                $url= get_post_meta( get_the_ID(), '_video_link', true ); 
                $url=str_replace("watch?v=", "embed/", $url);
            ?>
    <iframe width="390" height="200" src="<?php echo $url; ?>">
    </iframe>
    <div>
        <h4>
            <?php echo get_the_title(); ?>
        </h4>
        <p>
            <?php echo get_the_content(); ?>
        </p>
    </div>


</li>

<?php } 
}
add_shortcode('capacitaciones', 'capacitaciones');

function listas_n_e(){
    global $post;
    $args = array(
        'post_type'             => 'noticias-post',
        'post_status'           => 'publish',
        'ignore_sticky_posts'   => 1,
        'posts_per_page'        => -1,
        'orderby'     => 'date',
        'order'       => 'DESC' ,
 
     );

$query = new WP_Query( $args );
// The Loop
?>
<div class="noti-event tal">
    <h3>
        Noticias y eventos recientes
    </h3>
    <ul class="tal">
        <?php
while ( $query->have_posts() ) {
    $query->the_post();?>
        <li>
            <a href="<?php echo get_permalink(); ?>">
                <?php echo get_the_title(); ?>
            </a>
        </li>

        <?php } 
?>
    </ul>
</div>
<?php
}
add_shortcode('listas_n_e', 'listas_n_e');

?>