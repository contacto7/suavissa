<?php 


add_action( 'cmb2_init', 'metabox_nosotros' );
function metabox_nosotros() {
$prefix = '_info_';
    
    $cmb = new_cmb2_box( array(
        'id'           => $prefix . 'nosotros',
        'title'        => 'Información',
        'object_types' => array( 'page'),
        'show_on' => array( 'key' => 'page-template', 'value' => 'nosotros.php' ),
        'context'      => 'normal',
        'priority'     => 'default',
    ) );

    $cmb->add_field( array(
        'name' => 'Descripción breve',
        'id' => $prefix . 'intro',
        'type' => 'textarea',
    ) );    

}

add_action( 'cmb2_init', 'metabox_home' );
function metabox_home() {
    $prefix = '_marcas_';
        
        $cmb = new_cmb2_box( array(
            'id'           => $prefix . 'marcas',
            'title'        => 'Marcas',
            'object_types' => array( 'page'),
            'show_on' => array( 'key' => 'page-template', 'value' => 'homepage.php' ),
            'context'      => 'normal',
            'priority'     => 'default',
        ) );
    
        $cmb->add_field( array(
            'name' => 'Marcas',
            'id' => $prefix . '_marca',
            'type' => 'file_list',
        ) );
}


add_action( 'cmb2_init', 'metabox_videos' );
function metabox_videos() {
$prefix = '_video_';
    
    $cmb = new_cmb2_box( array(
        'id'           => $prefix . 'videos',
        'title'        => 'Link del video',
        'object_types' => array( 'post', 'video-post' ),
        'context'      => 'normal',
        'priority'     => 'default',
    ) );

    $cmb->add_field( array(
        'name' => 'Link',
        'id' => $prefix . 'link',
        'type' => 'text_url',
    ) );    

}
?>