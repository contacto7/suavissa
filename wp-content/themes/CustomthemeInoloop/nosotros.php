<?php
/**
 * Template Name: Nosotros
 *

 */

get_header();
?>

<div>
    <main class="grid cx">

        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <div class="banner">
            <?php
            echo do_shortcode('[smartslider3 slider="2"]');
            ?>
        </div>


        <div class="content">
            <section>
                <div class="titulo">
                    <h2>SOBRE NOSOTROS</h2>
                    <h3>
                        Lo mejor para ti</h3>
                </div>
            </section>
            <section class="hist grid cx cy">
                <img src="/wp-content/uploads/2020/06/Nosotros-inicio.png" alt="">

                <div>
                    <p>
                        <?php echo get_post_meta( get_the_ID(), '_info_intro', true ); ?>

                    </p>

                </div>
            </section>

            <section class="val-org grid cx cy">
                <div>
                    <h3>
                        VALORES ORGANIZACIONALES
                    </h3>
                    <p>
                        Nuestra cultura es el de compartir información de calidad, con persona que quieran aprender y
                        asimismo brinden información, con el objetivo de crecer personalmente y en equipo, para lo cual
                        informamos y orientamos a nuestro cliente en el reconcimiento de la calidad y aplicaciones de
                        los
                        materiales que comerzalizamos
                    </p>
                    <p>
                        <span>CONFIANZA:</span> Damos la confianza a nuestros cliente, colaboradores mediante el
                        resultado
                        de
                        nuestra gestión.</p>
                    <p>
                        <span>INTEGRIDAD:</span> Brindamos a nuestros clientes y colaboradores, calidad, variedad y
                        seguridad en las
                        gestiones y productos que comercializamos.</p>
                    <p>
                        <span>RESPETO:</span> A los proveedores, clientes y colaboradores.</p>

                </div>

                <img src="/wp-content/uploads/2020/06/Nosotros.png" alt="fabrica toallas peru lima">


            </section>
            <section class="valores grid cx">
                <div>
                    <h3>CONTROL</h3>
                    <p>Dada la importancia de la calidad dentro del exigente
                        mercado existente, la empresa cuenta con un estricto
                        control de calidad que permita el cumplimiento de las
                        especificaciones solicitadas por cada cliente</p>
                </div>
                <div>
                    <h3>VISIÓN</h3>
                    <p>Posicionamos sólidamente en el mercado ofreciendo
                        productos de calidad a precios competitivos.</p>
                </div>
                <div>
                    <h3>MISIÓN</h3>
                    <p>Todos nuestros esfuerzos están enfocados a la
                        identificación y satisfacción de las necesidades de los
                        consumidores, contribuyendo de está forma a
                        mejorar la de vida de las personas.</p>
                </div>
            </section>
            <section class="galeria">
                <div class="titulo grid cx">
                    <h3 class="w100">
                        NUESTRA GALERÍA
                    </h3>
                    <h4> Ambientes de nuestra fábrica</h4>
                </div>
                <?php
            echo do_shortcode('[modula id="75"]');
            ?>

            </section>
        </div>


        <?php endwhile; ?>
        <?php endif; ?>


    </main><!-- .site-main -->
</div><!-- .content-area -->

<?php
get_footer();