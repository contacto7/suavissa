<?php
/**
 * Template Name: Inicio
 *

 */

get_header();
?>

<div>
    <main class="grid cx">

        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <div class="banner">
            <?php
                    echo do_shortcode('[smartslider3 slider="1"]');
                ?>
        </div>

        <?php endwhile; ?>
        <?php endif; ?>
        <div class="content">




            <section class="tac">

                <div class="titulo">
                    <h2>CATEGORÍAS</h2>
                </div>
                <div class="categories grid w100 cx">
                    <?php        
                 echo do_shortcode('[categories_shop]')
                ?>
                </div>
                <a href="/tienda" class="button">
                    Ir a la tienda</a>
            </section>
        </div>
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        <div class="content">
            <section class="marcas grid cx">
                <div class="titulo w100">
                    <h2>NUESTRAS MARCAS</h2>
                    <p>Entérate de las marcas que manejamos para nuestros productos</p>
                </div>
                <?php
                $html='';
                        $files = get_post_meta( get_the_ID(), '_marcas__marca', 1 );
                        foreach ( (array) $files as $attachment_id => $attachment_url ) {
                            $html.= '<img src="'. wp_get_attachment_image_src($attachment_id)[0] .'" alt="marcas de suavissa toallas en Perú" >';
                        }
                        echo $html;
                ?>

            </section>
        </div>

        <?php endwhile; ?>
        <?php endif; ?>
        <div class="minibanner">

        </div>
        <section class="woocommerce tac container-slick">
            <div class="titulo">
                <h2>PRODUCTOS DESTACADOS</h2>
                <h3>Lo mejor para ti</h3>
            </div>
            <ul class="products products-slick">
                <?php     
                        echo do_shortcode('[productos_destacados]');                    
                        //echo do_shortcode('[productos_cat term="16"]')
                    ?>

            </ul>
            <a href="/tienda" class="button">
                Ir a la tienda
            </a>
        </section>
        <section class="woocommerce tac container-slick">
            <div class="titulo">
                <h2>PRENDAS PARA BAÑO</h2>
            </div>
            <ul class="products products-slick">
                <?php     
                        echo do_shortcode('[productos_cat term="108"]');
                ?>
            </ul>
            <a href="/tienda" class="button">
                Ir a la tienda
            </a>
        </section>



        <div class="content">
            <section class="news">
        
                    <?php     
                        echo do_shortcode('[noticias_eventos]');                    
                    ?>
            </section>
        </div>
        <br/>
        <br/>


    </main><!-- .site-main -->
</div><!-- .content-area -->

<?php
get_footer();