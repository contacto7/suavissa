<aside class="sidebar">
    <button id="toggleb">
        <img src="<?php echo get_template_directory_uri()."/images/filtro.png"; ?>" alt="filtro">
    </button>
    <div id="custom-sidebar" itemtype="https://schema.org/WPSideBar" itemscope="itemscope"
        class="widget-area sidebar grid cx">
        <?php if ( is_active_sidebar( 'news-widget-area' ) ) : ?>
        <?php dynamic_sidebar( 'news-widget-area' ); ?>
        <?php endif; ?>
    </div>
</aside>