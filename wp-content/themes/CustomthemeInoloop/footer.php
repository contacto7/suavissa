<?php
/**
 */

?>

</div><!-- #content -->

<footer class="grid cx">
    <div class="content grid cy">
        <a class="logo" href="/">
            <img src="<?php echo get_template_directory_uri()."/images/logo.png"; ?>" alt="logo">
        </a>
        <div>
            <p>
                <b>
                    Teléfono:
                </b>
            </p>
            <p>
                01-3244970 / 998-148-883
            </p>
            <p>998148883 / 942086715 / 998145221</p>

            <p>
                <b>
                    Dirección:
                </b>
            </p>
            <p>
                JIRÓN ANTONIO BAZO N°578 - LA VICTORIA / JIRÓN ANTONIO BAZO N°635 - LA VICTORIA
            </p>
            <p>CALLE 4 MZ.G LT.2 URB. LAS VEGAS - PUENTE PIEDRA</p>
            <p>
                <b>
                    Facebook:
                </b>
            </p>
            <a href="/">
                Toallas Suavisa y Más
            </a>
        </div>
    </div>
    <div class="w100 cpr">
        &copy; Copyright SUAVISSA 2020 - Desarrollado por <a href="https://inoloop.com/">Inoloop</a>
    </div>
</footer>

</div><!-- #page -->

<?php wp_footer(); ?>
<script type="text/javascript" src="/wp-content/themes/CustomthemeInoloop/js/slick.js"></script>
<script>
jQuery(document).ready(function() {
    var screen = jQuery(window)
    jQuery("#toggle-m").on("click", function() {
        jQuery(this).toggleClass("active");
        jQuery(this).siblings(".mob").toggle();
    });
    jQuery("#toggleb").on("click", function() {
        jQuery(this).toggleClass("active");
        jQuery(this).siblings("div").toggle();
    });
    if (screen.width() < 1200) {
        jQuery("#content .content").on("click", function() {
            jQuery(this).siblings("div").children(".w100").hide();
        });
    }

    /*SLICK*/
    jQuery('.products-slick').slick({

        infinite: false,
        slidesToShow: 4,
        slidesToScroll: 1,
        dots: true,
        autoplay: true,
        autoplaySpeed: 2500,
        infinite: true,
        arrows: true,
        responsive: [{
                breakpoint: 1080,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 780,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1
                }
            }
        ]


    });
    /*SLICK*/

});
</script>
</body>

</html>