<?php
/**
 * The main template file
 *
 * @link https://www.inoloop.com/
 * @since 1.0.0
 */

get_header();
?>

<div>
    <main class="grid cx">
        <?php get_sidebar(); ?>

        <div class="content">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <h1><?php the_title(); ?></h1>
            <div>
                <?php the_content(); ?>
            </div>

            <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </main><!-- .site-main -->
</div><!-- .content-area -->

<?php
get_footer();