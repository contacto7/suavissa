<?php
/**

 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap" rel="stylesheet">    
    <link href="/wp-content/themes/CustomthemeInoloop/css/slick.css" rel="stylesheet">
    <link href="/wp-content/themes/CustomthemeInoloop/css/slick-theme.css" rel="stylesheet">
    <title><?php bloginfo('name'); ?><?php wp_title(); ?></title>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <?php wp_body_open(); ?>
    <nav class="grid cx">
        <div class="up content grid cy cxb">
            <a class="logo" href="/">
                <img src="<?php echo get_template_directory_uri()."/images/logo.png"; ?>" alt="logo">
            </a>
            <div class="grid cx cys">
                <?php get_search_form( ); ?>
                <a href="/cart" class="tac carrito">
                    <div class="">
                        <img src="<?php echo get_template_directory_uri()."/images/cart.png"; ?>" alt="carrrito">
                    </div>
                    <p>Carrito</p>
                </a>
            </div>
        </div>
        <div class="down w100 grid cx">
            <div class="desk">
                <?php
                wp_nav_menu( array( 
                'theme_location' => 'header-menu', 
                'container_class' => 'navigation' ) ); 
            ?>
            </div>
            <button id="toggle-m" class="toggle-m grid cx">
                <span></span>
                <span></span>
                <span></span>
            </button>
            <div class="mob">
                <?php
                wp_nav_menu( array( 
                    'theme_location' => 'header-menu', 
                    'container_class' => 'navigation' ) ); 
            ?>
            </div>
        </div>

    </nav>
    <div>
        <div id="content" class="site-content">