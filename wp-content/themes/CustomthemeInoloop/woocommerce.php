<?php get_header(); ?>
<main id="content" class="grid cx" role="main">
    <div class="content">
        <?php woocommerce_content(); ?>
    </div>
    <?php
    if(is_shop()){
        get_sidebar();
    }
 ?>

</main>
<?php get_footer(); ?>