<?php 
add_theme_support( 'post-thumbnails' );
add_theme_support( 'menus' );
add_theme_support( 'widgets' );
add_theme_support( 'wc-product-gallery-lightbox' );
add_theme_support( 'wc-product-gallery-zoom' );
add_theme_support( 'wc-product-gallery-slider' );
remove_action( 'woocommerce_sidebar',             'woocommerce_get_sidebar', 10 );


remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
add_action('woocommerce_before_main_content', 'my_theme_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'my_theme_wrapper_end', 10);

function my_theme_wrapper_start() {
    echo '<section id="main">';
}

function my_theme_wrapper_end() {
    echo '</section>';
}
function store_sidebar() {
    register_sidebar(
        array (
            'name' => 'Tienda',
            'id' => 'primary-widget-area',
            'description' => 'Tienda Sidebar',
            'before_widget' => '<section id="%1$s" class="widget %2$s widget-content">',
            'after_widget' => "</section>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
}
add_action( 'widgets_init', 'store_sidebar' );


function news_sidebar() {
    register_sidebar(
        array (
            'name' => 'Noticias y eventos',
            'id' => 'news-widget-area',
            'description' => 'News Sidebar',
            'before_widget' => '<section id="%1$s" class="widget %2$s widget-content">',
            'after_widget' => "</section>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
}
add_action( 'widgets_init', 'news_sidebar' );

function my_theme_setup() {
    add_theme_support( 'woocommerce' ); 
}
add_action( 'after_setup_theme', 'my_theme_setup' ); 

add_filter('loop_shop_columns', 'loop_columns', 999);
if (!function_exists('loop_columns')) {
	function loop_columns() {
		return 5; // 5 products per row
	}
}
function custom_menu() {
    register_nav_menus(
      array(
        'header-menu' => __( 'Header Menu' )
      )
    );
}
add_action( 'init', 'custom_menu' );
include get_template_directory() . '/inc/shortcode.php';
include get_template_directory() . '/inc/custom-posts.php';
include get_template_directory() . '/inc/shortcode-shop.php';
include get_template_directory() . '/inc/metaboxes.php';



?>