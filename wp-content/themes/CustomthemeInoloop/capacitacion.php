<?php
/**
 * Template Name: Capacitación
 *

 */

get_header();
?>

<div>
    <main class="grid cx">

        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <div class="banner">
            <?php
echo do_shortcode('[smartslider3 slider="3"]');
?>
        </div>


        <div class="content">
            <section>
                <div class="titulo">
                    <h2>CAPACITACIÓN</h2>
                    <h3>
                        Nuestra lista de videos útiles</h3>
                </div>
            </section>
            <section class="videos grid cx cy">
                <?php     
                        echo do_shortcode('[capacitaciones]');                    
                ?>

            </section>
        </div>
        <?php endwhile; ?>
        <?php endif; ?>


    </main><!-- .site-main -->
</div><!-- .content-area -->

<?php
get_footer();